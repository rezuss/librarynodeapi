const mongoose = require('mongoose');

const UserSchema = mongoose.Schema({
	email: {
		type: String,
		required: true
	},
	password: {
		type: String,
		required: true
	},
	role: {
		type: String,
		required: true
	},
	firstname: {
		type: String,
		required: false
	},
	lastname: {
		type: String,
		required: false
	},
	registerdatetime: {
		type: Date,
		default: Date.now
	}
});

module.exports = mongoose.model('Users', UserSchema);