const mongoose = require('mongoose');
const User = require('./User');
const Book = require('./Book');

const ReservationSchema = mongoose.Schema({
	user: {
		type: mongoose.Schema.Types.ObjectId,
		ref: User,
		required: true
	},
	book: {
		type: mongoose.Schema.Types.ObjectId,
		ref: Book,
		required: true
	},
	date_start: {
		type: Date,
		default: Date.now
	},
	date_end: {
		type: Date,
		required: false
	}
});

module.exports = mongoose.model('Reservations', ReservationSchema);