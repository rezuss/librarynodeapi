const mongoose = require('mongoose');

const BookSchema = mongoose.Schema({
	title: {
		type: String,
		required: true
	},
	author: {
		type: String,
		required: true
	},
	publisher: {
		type: String,
		required: true
	},
	publishedyear: {
		type: Number,
		required: true
	},
	description: {
		type: String,
		required: false
	},
	language: {
		type: String,
		required: true
	},
	category: {
		type: String,
		required: true
	},
	location: {
		type: String,
		required: true
	}
});

module.exports = mongoose.model('Books', BookSchema);