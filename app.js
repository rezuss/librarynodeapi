const express = require('express');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const cors = require('cors');
const session = require('express-session');
const jwt = require('jsonwebtoken');
require('dotenv/config');

const authRoute = require('./routes/Auth');
const usersRoute = require('./routes/Users');
const booksRoute = require('./routes/Books');
const reservationsRoute = require('./routes/Reservations');

const app = express();

const verifyJWT = async (req, res, next) => {
	const token = req.headers['x-access-token'];
	if (!token) {
		res.json({ message: 'You need auth token to continue.' });
	} else {
		jwt.verify(token, process.env.JWT_SECRET, async (err, decoded) => {
			if (err) {
				res.json({ message: 'Failed to authenticate.' });
			} else {
				next();
			}
		});
	}
};

app.use(cors());
app.use(bodyParser.json());
app.use(session({
	key: "userID",
	secret: process.env.SESSION_SECRET,
	resave: false,
	saveUninitialized: false,
	cookie: { expires: 60 * 60 * 24 }
}));

app.use('/', authRoute);
app.use('/users', verifyJWT, usersRoute);
app.use('/books', verifyJWT, booksRoute);
app.use('/reservations', verifyJWT, reservationsRoute);

app.get('/', (req, res) => {
	res.send('Welcome to Library Node API!');
});

mongoose.connect(process.env.DB_CONNECTION,
	{
		useNewUrlParser: true,
		useUnifiedTopology: true
	}).then(() => {
		console.log('Connected to DB!');
	}).catch(err => {
		console.error(err);
	});

app.listen(5000);