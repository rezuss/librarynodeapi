const express = require('express');

const Reservation = require('../models/Reservation');
const User = require('../models/User');
const Book = require('../models/Book');

const router = express.Router();

// Reservations list
router.get('/', async (req, res) => {
	try {
		const reservations = await Reservation.find();
		if (reservations.length > 0) {
			res.json(reservations);
		} else {
			res.json({ message: 'There are no reservations.' });
		}
	} catch (err) {
		res.json({ message: 'Unknown error.', error: err });
	}
});

// Get reservation by ID
router.get('/:id', async (req, res) => {
	try {
		const reservation = await Reservation.findById(req.params.id);
		res.json(reservation);
	} catch (err) {
		res.json({ message: err });
	}
});

// Get reservations by book
router.get('/book/:id', async (req, res) => {
	try {
		const book = await Book.findById(req.params.id);
		const reservations = await Reservation.find({ $or: [{ 'book': book }] });
		if (reservations.length > 0) {
			res.json(reservations);
		} else {
			res.json({ message: 'There are no reservations for that book.' });
		}
	} catch (err) {
		res.json({ message: 'There is no such book.', error: err });
	}
});

// Get reservations by user
router.get('/user/:id', async (req, res) => {
	try {
		const user = await User.findById(req.params.id);
		const reservations = await Reservation.find({ $or: [{ 'user': user }] });
		if (reservations.length > 0) {
			res.json(reservations);
		} else {
			res.json({ message: 'There are no reservations for that user.' });
		}
	} catch (err) {
		res.json({ message: 'There is no such user.', error: err });
	}
});

// Add reservation
router.post('/', async (req, res) => {
	try {
		const book = await Book.findById(req.body.book_id);
		const reservations = await Reservation.find({ $or: [{ 'book': book, 'date_end': null }] });
		if (reservations.length == 0) {
			try {
				const user = await User.findById(req.body.user_id);
				const res_count = await Reservation.find({ $or: [{ 'user': user, 'date_end': null }] });
				if (res_count.length < 5) {
					const reservation = new Reservation({
						user: user,
						book: book
					});
					try {
						const newReservation = await reservation.save();
						res.json(newReservation);
					} catch (err) {
						res.json({ message: 'Error with creating new reservation.', error: err });
					}
				} else {
					res.json({ message: 'This user has reached the maximum number of reservations.' });
				}
			} catch (err) {
				res.json({ message: 'There is no such user.', error: err });
			}
		} else {
			res.json({ message: 'This book is already reserved.' });
		}
	} catch (err) {
		res.json({ message: 'There is no such book.', error: err });
	}
});

// Delete reservation
router.delete('/:id', async (req, res) => {
	try {
		const removedReservation = await Reservation.deleteOne({ _id: req.params.id });
		res.json(removedReservation);
	} catch (err) {
		res.json({ message: 'There is no such reservation.', error: err });
	}
});

// Update reservation
router.patch('/:id', async (req, res) => {
	try {
		const reservation = await Reservation.findById(req.params.id);
		const user = reservation.user;
		const book = reservation.book;
		try {
			const newUser = await User.findById(req.body.user_id);
			try {
				const newBook = await Book.findById(req.body.book_id);
				const updatedReservation = await Reservation.updateOne(
					{ _id: req.params.id },
					{
						$set: {
							user: newUser || user,
							book: newBook || book,
							date_end: req.body.date_end || null
						}
					}
				);
				res.json(updatedReservation);
			} catch (err) {
				res.json({ message: 'There is no such book.', error: err });
			}
		} catch (err) {
			res.json({ message: 'There is no such user.', error: err });
		}
	} catch (err) {
		res.json({ message: 'There is no such reservation.', error: err });
	}
});

module.exports = router;