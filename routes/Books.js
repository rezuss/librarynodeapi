const express = require('express');

const Book = require('../models/Book');

const router = express.Router();

// Books list
router.get('/', async (req, res) => {
	try {
		const books = await Book.find();
		if (books.length > 0) {
			res.json(books);
		} else {
			res.json({ message: 'There are no books.' });
		}
	} catch (err) {
		res.json({ message: 'Unknown error.', error: err });
	}
});

// Get book by ID
router.get('/:id', async (req, res) => {
	try {
		const book = await Book.findById(req.params.id);
		res.json(book);
	} catch (err) {
		res.json({ message: 'There is no such book.', error: err });
	}
});

// Add book
router.post('/', async (req, res) => {
	const book = await Book.findOne({ $or: [{ 'title': req.body.title }] });
	if (book === null) {
		const book = new Book({
			title: req.body.title,
			author: req.body.author,
			publisher: req.body.publisher,
			publishedyear: req.body.publishedyear,
			description: req.body.description,
			language: req.body.language,
			category: req.body.category,
			location: req.body.location,
			available: req.body.available
		});
		try {
			const newBook = await book.save();
			res.json(newBook);
		} catch (err) {
			res.json({ message: 'Error with creating new book.', error: err });
		}
	} else {
		res.json({ message: "Book with this title already exists." });
	}
});

// Delete book with reservations
router.delete('/:id', async (req, res) => {
	try {
		const book = await Book.findById(req.params.id);
		try {
			const removedBook = await Book.deleteOne({ _id: book._id });
			try {
				const reservations = await Reservation.find();
				if (reservations.length > 0) {
					reservations.deleteMany({ book: book }, (err, result) => {
						if (!err) {
							res.json({ removedBook: removedBook, removedReservations: result });
						} else {
							res.json({ message: 'Error deleting book reservations.', error: err });
						}
					});
				}
			} catch (err) {
				res.json({ message: 'Error finding book reservations.', error: err });
			}
		} catch (err) {
			res.json({ message: 'Error deleting book.', error: err });
		}
	} catch (err) {
		res.json({ message: 'There is no such book.', error: err });
	}
});

// Update book
router.patch('/:id', async (req, res) => {
	try {
		const book = await Book.findById(req.params.id);
		const anotherBook = await Book.findOne({ $or: [{ 'title': req.body.title }] });
		if (anotherBook === null || anotherBook._id == req.params.id) {
			const updatedBook = await Book.updateOne(
				{ _id: req.params.id },
				{
					$set: {
						title: req.body.title || book.title,
						author: req.body.author || book.author,
						publisher: req.body.publisher || book.publisher,
						publishedyear: req.body.publishedyear || book.publishedyear,
						description: req.body.description || book.description,
						language: req.body.language || book.language,
						category: req.body.category || book.category,
						location: req.body.location || book.location,
						available: req.body.available || book.available
					}
				}
			);
			res.json(updatedBook);
		} else {
			res.json({ message: 'This title is already in use.' });
		}
	} catch (err) {
		res.json({ message: 'There is no such book.', error: err });
	}
});

module.exports = router;