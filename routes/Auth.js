const express = require('express');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

const User = require('../models/User');

const router = express.Router();

// Login
router.post('/auth', async (req, res) => {
	try {
		const user = await User.findOne({ $or: [{ 'email': req.body.email }] });
		bcrypt.compare(req.body.password, user.password, (err, result) => {
			if (err) {
				res.json({ message: 'Error with password hash.', error: err });
			} else {
				if (result) {
					req.session.user = user;
					const token = jwt.sign({ id: user._id }, process.env.JWT_SECRET, { expiresIn: 300 });
					res.json({
						message: 'Authorized',
						user: {
							id: user._id,
							email: user.email,
							role: user.role,
							firstname: user.firstname,
							lastname: user.lastname,
							token: token
						}
					});
				} else {
					res.json({ message: 'Unauthorized' });
				}
			}
		});
	} catch (err) {
		res.json({ message: 'There is no such user.', error: err });
	}
});

// Register
router.post('/register', async (req, res) => {
	const user = await User.findOne({ $or: [{ 'email': req.body.email }] });
	if (user === null) {
		bcrypt.hash(req.body.password, 10, async (err, hash) => {
			if (err) {
				res.json({ message: 'Error with password hash.', error: err });
			} else {
				const user = new User({
					email: req.body.email,
					password: hash,
					role: req.body.role,
					firstname: req.body.firstname,
					lastname: req.body.lastname
				});
				try {
					const newUser = await user.save();
					res.json(newUser);
				} catch (err) {
					res.json({ message: 'Error with creating new user.', error: err });
				}
			}
		});
	} else {
		res.json({ message: "User with this email already exists." });
	}
});

module.exports = router;