const express = require('express');
const bcrypt = require('bcrypt');

const User = require('../models/User');
const Reservation = require('../models/Reservation');

const router = express.Router();

// Users list
router.get('/', async (req, res) => {
	try {
		const users = await User.find();
		if (users.length > 0) {
			res.json(users);
		} else {
			res.json({ message: 'There are no users.' });
		}
	} catch (err) {
		res.json({ message: 'Unknown error.', error: err });
	}
});

// Get user by ID
router.get('/:id', async (req, res) => {
	try {
		const user = await User.findById(req.params.id);
		res.json(user);
	} catch (err) {
		res.json({ message: 'There is no such user.', error: err });
	}
});

// Delete user
router.delete('/:id', async (req, res) => {
	try {
		const user = await User.findById(req.params.id);
		try {
			const removedUser = await User.deleteOne({ _id: user._id });
			try {
				const reservations = await Reservation.find();
				if (reservations.length > 0) {
					reservations.deleteMany({ user: user }, (err, result) => {
						if (!err) {
							res.json({ removedUser: removedUser, removedReservations: result });
						} else {
							res.json({ message: 'Error deleting user reservations.', error: err });
						}
					});
				}
			} catch (err) {
				res.json({ message: 'Error finding user reservations.', error: err });
			}
		} catch (err) {
			res.json({ message: 'Error deleting user.', error: err });
		}
	} catch (err) {
		res.json({ message: 'There is no such user.', error: err });
	}
});

// Update user
router.patch('/:id', async (req, res) => {
	try {
		const user = await User.findById(req.params.id);
		const anotherUser = await User.findOne({ $or: [{ 'email': req.body.email }] });
		if (anotherUser === null || anotherUser._id == req.params.id) {
			var hashPass = null;
			bcrypt.hash(req.body.password, 10, async (err, hash) => {
				if (!err) {
					hashPass = hash;
				}
			});
			const updatedUser = await User.updateOne(
				{ _id: req.params.id },
				{
					$set: {
						email: req.body.email || user.email,
						password: hashPass || user.password,
						role: req.body.role || user.role,
						firstname: req.body.firstname || user.firstname,
						lastname: req.body.lastname || user.lastname
					}
				}
			);
			res.json(updatedUser);
		} else {
			res.json({ message: 'Email is already taken.' });
		}
	} catch (err) {
		res.json({ message: 'There is no such user.', error: err });
	}
});

module.exports = router;